<?php
session_start();
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title> URL Shortener </title>
    </head>
    <body>
    <h2>Url shortener</h2>
    <?php
    if(isset($_SESSION['message'])) {
        // Message containing short url
        echo "<p>{$_SESSION['message']}</p>";

        // Message that contains statistsics for that url
        echo "<p>Last accessed:{$_SESSION['last_accessed']}</p>";
        echo "<p>Number of visits:{$_SESSION['visits']}</p>";

        unset($_SESSION['message']);
    }
    ?>
    <form action="shorten.php" method="post">
        <input type="url" name="url">
        <input type="submit" value="Submit">
    </form>
    </body>
</html>