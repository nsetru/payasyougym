<?php

require_once 'class/urlshortenerclass.php';


// var_dump($_GET);
if(isset($_GET['shortcode'])) {
    // Instantiate url_shortner class
    $urlshortener = new urlshortenerclass();
    $shortcode = $_GET['shortcode'];

    // Get coresponding long url
    if($longurl = $urlshortener->getLongUrl($shortcode)) {
        // Update last accessed and visits
        $urlshortener->updateStats($shortcode);

        // redirect to original url and exit
        header("Location: {$longurl}");
        die();
    }
}

// If above doesn't work redirect to index.php
header("Location: index.php");