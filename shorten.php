<?php
session_start();
require 'class/urlshortenerclass.php';

$shortner = new urlshortenerclass();

if(isset($_POST['url'])) {
    $longurl = $_POST['url'];

    if ($shortcode = $shortner->makeShortUrl($longurl)) {
        //echo $shortcode;
        $_SESSION['message'] = "The short url is <a href=\"http://localhost/~nivedita/payasyougym/urlshortner-test-be/{$shortcode}\">http://localhost/~nivedita/payasyougym/urlshortner-test-be/{$shortcode}</a>";

        // Get stats
        $stats = $shortner->getStats($longurl);
        $_SESSION['last_accessed'] = $stats->last_accessed;
        $_SESSION['visits'] = $stats->visits;
    } else {
        $_SESSION['message'] = 'Error..!';

    }
}

header('Location: index.php');

