# Backend test - Url Shortner

We'd like you to create a URL shortening service, using an sqlite database.

There should be an input field and submit button on the page into which the URL you would like to shorten can be entered.
A short url should be returned upon submitting the form.

When visiting the short url it should redirect you to the previously entered url.

On the home page of the url shortner it would be great to see some sort of statistic, what statistic this is and how you display this is up to you.

You can use whatever PHP framework you are comfortable with. 

We are not concerned about how the frontend looks as long as the functionality is in place.

Please don't spend more than a few hours on this.

From a functionality side, the 3 points we will be looking at are:

1. Can we post a url in a form and does it return a shortened url
2. Does the shortned url redirect to the posted url
3. Does the statistic update when I do a relevant action

## Submission

Please clone this repository, write your code and update this README with a guide of how to run it.

Either send us a link to the repository on somewhere like github or bitbucket (bitbucket has free private repositories).



###### Nivedita Test Solution #######

File structure
- index.php : Display html form
- Shorten.php : Is the action script called from form. Makes relevant calls to generate shortlink
- Class/urlshortnerclass.php : Has all public, protected funtions to make db calls, generate shorturlcode, update stats and return shorturl and stats
- redirect.php : Script to perform proper redirection given the short url
- .htaccess : rewriterule to redirect script
- database/database.sql : create table structure
