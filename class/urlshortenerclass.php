<?php

/**
 * Class urlshortenerclass
 */

class urlshortenerclass {

    /**
     * @var mysqli
     */
    protected $database;

    /**
     * urlshortenerclass constructor.
     */
    public function __construct()
    {
        // TODO :: move this code to separate db class
        $this->database = new mysqli('127.0.0.1', 'root', 'password', 'payasyougym');
    }

    /**
     * @param $insertid
     * @param $length
     * @return string
     */
    protected function generateShortUrl($insertid, $length){

        // Perform base conversion using insert id to make sure the generated short_url is unique for each long url
        // base conversion from base 10 to base 36 - gives us combination of alpha numeric characters
        $str1 = base_convert($insertid, 10, 36);

        // Generate a random string for str2
        $characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $charsLength = strlen($characters) -1;
        $str2 = "";

        // The length of generated code shouldn't exceed length.
        // So subtract the length of str1 from length and generate random string for rest of the characters
        $length = $length - strlen($str1);
        for($i=0; $i<$length; $i++){
            $randNum = mt_rand(0, $charsLength);
            $str2 .= $characters[$randNum];
        }

        // Concatenate str1 and str2 to get complete random string of specified length
        $string = $str1.$str2;

        return $string;
    }

    /**
     * @param $longurl
     * @return string
     */
    public function makeShortUrl($longurl) {

        $longurl = trim($longurl);


        // Validate url
        if(!filter_var($longurl, FILTER_VALIDATE_URL)) {
            return '';
        }

        // escape special characters from url if any
        $longurl = $this->database->real_escape_string($longurl);

        // Query to get short url
        $query = "SELECT short_url from url_shortener where long_url = '{$longurl}'";
        $result = $this->database->query($query);

        // check if url already exists
        if($result->num_rows) {

            while($obj = $result->fetch_object()) {
                return $obj->short_url;
            }

        } else {
            // If url doesn't exists - insert record without short_url
            $ins_query = "INSERT INTO url_shortener (long_url, created, last_accessed, visits) VALUES ('{$longurl}',now(), now(), 0)";
            $result_ins = $this->database->query($ins_query);

            // Generate short url code
            $length = 6;
            $short_url = $this->generateShortUrl($this->database->insert_id, $length);

            // Update the record with generated short_url
            $upt_query = "UPDATE url_shortener SET short_url = '{$short_url}' WHERE long_url='{$longurl}'";
            $result_upt = $this->database->query($upt_query);

            return $short_url;
        }

    }

    /**
     * @param $shortcode
     * @return string
     */
    public function getLongUrl($shortcode) {
        $shortcode = $this->database->real_escape_string($shortcode);

        $query = "SELECT long_url from url_shortener WHERE short_url = '{$shortcode}'";
        $result = $this->database->query($query);

        if($result->num_rows) {
            while($obj = $result->fetch_object()) {
                return $obj->long_url;
            }
        }

        return '';
    }

    /**
     * Update last accessed and visits to the url to get some stats
     * @param $shortcode
     */
    public function updateStats($shortcode) {
        $shortcode = $this->database->real_escape_string($shortcode);

        // retrive record for teh shortcode
        $query = "SELECT * from url_shortener WHERE short_url = '{$shortcode}'";
        $result = $this->database->query($query);

        if($result->num_rows) {
            $visits = $result->fetch_object()->visits;
        }

        // Increment visits and set current datetime for update
        $visits = $visits+1;
        $date = date('Y-m-d H:i:s');
        $update_query = "UPDATE url_shortener SET last_accessed = '{$date}', visits = '{$visits}' where short_url = '{$shortcode}'";
        $result_update = $this->database->query($update_query);
    }

    /**
     * Get last_accessed and vists for given url
     *
     * @param $longurl
     * @return object|stdClass
     */
    public function getStats($longurl) {
        $shortcode = $this->database->real_escape_string($longurl);

        $query = "SELECT * from url_shortener WHERE long_url = '{$longurl}'";
        $result = $this->database->query($query);

        if($result->num_rows) {
            return $result->fetch_object();
        }

    }
}