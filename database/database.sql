CREATE TABLE `url_shortener` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `long_url` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `short_url` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `last_accessed` datetime DEFAULT NULL,
  `visits` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
